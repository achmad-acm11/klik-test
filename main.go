package main

import (
	"klika2c-test/app"
	"klika2c-test/controller"
	"klika2c-test/helper"
	"klika2c-test/repository"
	"klika2c-test/service"
	"net/http"
	"os"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/joho/godotenv"
)

func main() {

	errEnv := godotenv.Load(".env")
	if errEnv != nil {
		panic("Failed to load env File. Make sure .env file is exists!")
	}

	app_port := os.Getenv("APP_PORT")

	// Config Database
	db := app.ConfigDB()

	// Init Validator
	validate := validator.New()

	// Item Repository
	itemRepository := repository.NewItemRepository()
	// Transaction Repository
	transactionRepository := repository.NewTransactionRepository()
	// Transaction Detail Repository
	transactionDetailRepository := repository.NewTransactionDetailRepository()
	// User Repository
	userRepository := repository.NewUserRepository(db)

	// Item Service
	itemService := service.NewItemService(itemRepository, db)
	// Transaction Service
	transactionService := service.NewTransactionService(transactionRepository, itemRepository, transactionDetailRepository, db)
	// User Service
	userService := service.NewUserService(userRepository)
	// Auth Service
	authService := service.NewAuthService()

	// Item Controller
	itemController := controller.NewItemController(itemService, validate)
	// Transaction Contoller
	transactionController := controller.NewTransactionController(transactionService, validate)
	// User Controller
	userController := controller.NewUserController(userService, authService, validate)

	// DEFAULT API
	router := gin.Default()

	api := router.Group("/api/v1")

	// Authorization Endpoint
	api.POST("/user", userController.LoginUser)
	api.POST("/register", userController.RegisterUser)

	// Item Endpoint
	api.GET("/item", itemController.GetItems)
	api.GET("/item/:id", itemController.GetItem)
	api.POST("/item", authMiddleware(userService, authService), itemController.CreateItem)
	api.PUT("/item/:id", authMiddleware(userService, authService), itemController.UpdateItem)
	api.DELETE("/item/:id", authMiddleware(userService, authService), itemController.DeleteItem)

	// Transaction Endpoint
	api.GET("/transaction", authMiddleware(userService, authService), transactionController.GetTransactions)
	api.GET("/transaction/:id", authMiddleware(userService, authService), transactionController.GetTransaction)
	api.POST("/transaction", authMiddleware(userService, authService), transactionController.CreateTransaction)
	api.PUT("/transaction/:id", authMiddleware(userService, authService), transactionController.UpdateTransaction)
	api.DELETE("/transaction/:id", authMiddleware(userService, authService), transactionController.DeleteTransaction)

	router.Run(":" + app_port)
}
func authMiddleware(u service.UserService, a service.AuthService) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")

		if !strings.Contains(authHeader, "Bearer") {
			response := helper.APIResponse("Unauthorized", "error", http.StatusUnauthorized, nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		var tokenString string

		arrayToken := strings.Split(authHeader, " ")

		if len(arrayToken) == 2 {
			tokenString = arrayToken[1]
		}

		valid, err := a.ValidateToken(tokenString)

		if err != nil || !valid.Valid {
			response := helper.APIResponse("Unauthorized", "error", http.StatusUnauthorized, nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		claim, ok := valid.Claims.(jwt.MapClaims)

		if !ok {
			response := helper.APIResponse("Unauthorized", "error", http.StatusUnauthorized, nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		id := int(claim["id"].(float64))
		userData, err := u.GetUserById(id)

		if err != nil || userData.Id == 0 {
			response := helper.APIResponse("Unauthorized", "error", http.StatusUnauthorized, nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		c.Set("currentUser", userData)
	}
}
