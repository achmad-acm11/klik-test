package controller

import (
	"klika2c-test/helper"
	"klika2c-test/request"
	"klika2c-test/response"
	"klika2c-test/service"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type userController struct {
	userService service.UserService
	authService service.AuthService
	validate    *validator.Validate
}

func NewUserController(u service.UserService, a service.AuthService, validate *validator.Validate) *userController {
	return &userController{u, a, validate}
}

// Handler for User Register
func (u *userController) RegisterUser(c *gin.Context) {

	// Format Input Data & Validasi
	var reqUserRegister request.RegisterUserInput

	// Get Input Data from type JSON
	c.ShouldBindJSON(&reqUserRegister)

	// Validation
	err := u.validate.Struct(reqUserRegister)

	if err != nil {
		response := helper.APIResponse("Incomplete Data", "error", http.StatusBadRequest, gin.H{"errors": err.Error()})
		c.JSON(http.StatusBadRequest, response)
		return
	}

	// Create User Data Service
	newUser, err := u.userService.RegisterUser(reqUserRegister)

	if err != nil {
		response := helper.APIResponse("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
		c.JSON(http.StatusInternalServerError, response)
		return
	}
	// Generate Token for auth JWT
	token, err := u.authService.GenerateToken(newUser.Id)

	if err != nil {
		response := helper.APIResponse("Failed generate token", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	// Update User for insert Token
	newUser.Token = token
	newUser, err = u.userService.UpdateUser(newUser)
	if err != nil {
		response := helper.APIResponse("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	response := helper.APIResponse("Register Success", "success", http.StatusOK, response.MapUserResponse(newUser))
	c.JSON(http.StatusOK, response)
}

// Handler for User Login
func (u *userController) LoginUser(c *gin.Context) {
	// Format Input Data & Validation
	var reqLogin request.LoginUserInput

	// Input Data From JSON
	c.ShouldBindJSON(&reqLogin)

	// Validate
	err := u.validate.Struct(reqLogin)

	if err != nil {
		response := helper.APIResponse("Login Failed", "error", http.StatusBadRequest, gin.H{"errors": err.Error()})
		c.JSON(http.StatusBadRequest, response)
		return
	}
	// Login Service
	loggedUser, err := u.userService.LoginUser(reqLogin)

	if err != nil {
		response := helper.APIResponse("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
		c.JSON(http.StatusInternalServerError, response)
		return
	}
	// Generate Token
	token, err := u.authService.GenerateToken(loggedUser.Id)
	if err != nil {
		response := helper.APIResponse("Failed generate token", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	loggedUser.Token = token

	response := helper.APIResponse("Login Success", "success", http.StatusOK, response.MapUserResponse(loggedUser))
	c.JSON(http.StatusOK, response)
}
