package controller

import (
	"klika2c-test/helper"
	"klika2c-test/request"
	"klika2c-test/service"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type itemController struct {
	service  service.ItemService
	validate *validator.Validate
}

func NewItemController(service service.ItemService, validate *validator.Validate) *itemController {
	return &itemController{
		service:  service,
		validate: validate,
	}
}

// Get Item All Handler
func (c *itemController) GetItems(ctx *gin.Context) {
	// Get Data List Item
	dataItems, err := c.service.GetAllItem(ctx)

	if err != nil {
		ctx.JSON(err.Meta.Code, err)
		return
	}

	response := helper.APIResponse("Success Get Items", "success", http.StatusOK, dataItems)
	ctx.JSON(http.StatusOK, response)
}

// Get Item One Handler
func (c *itemController) GetItem(ctx *gin.Context) {
	var itemId request.ItemRequestId

	ctx.ShouldBindUri(&itemId)

	// Get Data One Item
	dataItem, err := c.service.GetOneItem(ctx, itemId.Id)

	if err != nil {
		ctx.JSON(err.Meta.Code, err)
		return
	}

	response := helper.APIResponse("Success Get Item", "success", http.StatusOK, dataItem)
	ctx.JSON(http.StatusOK, response)
}

// Create Item Handler
func (c *itemController) CreateItem(ctx *gin.Context) {
	var reqItem request.ItemRequestJSON

	ctx.ShouldBindJSON(&reqItem)
	// Validate Format Request
	err := c.validate.Struct(reqItem)

	if err != nil {
		response := helper.APIResponse("Incomplete Data", "bad request", http.StatusBadRequest, gin.H{"errors": err.Error()})
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	dataItem, errResponse := c.service.CreateItem(ctx, reqItem)

	if errResponse != nil {
		ctx.JSON(errResponse.Meta.Code, err)
		return
	}

	response := helper.APIResponse("Success Create Item", "success", http.StatusOK, dataItem)
	ctx.JSON(http.StatusOK, response)

}

// Update Item Handler
func (c *itemController) UpdateItem(ctx *gin.Context) {
	var reqItem request.ItemRequestJSON

	var itemId request.ItemRequestId

	ctx.ShouldBindUri(&itemId)

	ctx.ShouldBindJSON(&reqItem)

	// Validate Format Request
	err := c.validate.Struct(reqItem)

	if err != nil {
		response := helper.APIResponse("Incomplete Data", "bad request", http.StatusBadRequest, gin.H{"errors": err.Error()})
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	dataItem, errResponse := c.service.UpdateItem(ctx, reqItem, itemId.Id)

	if errResponse != nil {
		ctx.JSON(errResponse.Meta.Code, errResponse)
		return
	}

	response := helper.APIResponse("Success Update Item", "success", http.StatusOK, dataItem)
	ctx.JSON(http.StatusOK, response)

}

// Delete Item Handler
func (c *itemController) DeleteItem(ctx *gin.Context) {

	var itemId request.ItemRequestId

	ctx.ShouldBindUri(&itemId)

	err := c.service.DeleteItem(ctx, itemId.Id)

	if err != nil {
		ctx.JSON(err.Meta.Code, err)
		return
	}

	response := helper.APIResponse("Success Delete Item", "success", http.StatusOK, gin.H{"message": "Delete Success"})
	ctx.JSON(http.StatusOK, response)

}
