package controller

import (
	"fmt"
	"klika2c-test/helper"
	"klika2c-test/request"
	"klika2c-test/service"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type transactionController struct {
	service  service.TransactionService
	validate *validator.Validate
}

func NewTransactionController(service service.TransactionService, validate *validator.Validate) *transactionController {
	return &transactionController{
		service:  service,
		validate: validate,
	}
}

// Get Transaction All Handler
func (c *transactionController) GetTransactions(ctx *gin.Context) {
	pagination := request.PaginationRequest{
		Page:     ctx.Query("page"),
		PageSize: ctx.Query("page_size"),
	}

	filter := request.TransactionFilter{
		Date:   ctx.Query("date"),
		Number: ctx.Query("number"),
	}

	fmt.Println(filter.Number)

	// Get Data List Transaction
	dataTransactions, err := c.service.GetAllTransaction(ctx, pagination, filter)

	if err != nil {
		ctx.JSON(err.Meta.Code, err)
		return
	}

	response := helper.APIResponse("Success Get Transactions", "success", http.StatusOK, dataTransactions)
	ctx.JSON(http.StatusOK, response)
}

// Get Transaction One Handler
func (c *transactionController) GetTransaction(ctx *gin.Context) {
	var transactionId request.TransactionRequestId

	ctx.ShouldBindUri(&transactionId)

	// Get Data One Transaction
	dataTransaction, err := c.service.GetOneTransaction(ctx, transactionId.Id)

	if err != nil {
		ctx.JSON(err.Meta.Code, err)
		return
	}

	response := helper.APIResponse("Success Get Transaction", "success", http.StatusOK, dataTransaction)
	ctx.JSON(http.StatusOK, response)
}

// Create Transaction Handler
func (c *transactionController) CreateTransaction(ctx *gin.Context) {
	var reqTransaction request.TransactionRequestJSON

	ctx.ShouldBindJSON(&reqTransaction)

	// Validate Format Request
	err := c.validate.Struct(reqTransaction)

	if err != nil {
		response := helper.APIResponse("Incomplete Data", "bad request", http.StatusBadRequest, gin.H{"errors": err.Error()})
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	dataTransaction, responseErr := c.service.CreateTransaction(ctx, reqTransaction)

	if responseErr != nil {
		ctx.JSON(responseErr.Meta.Code, responseErr)
		return
	}

	response := helper.APIResponse("Success Create Transaction", "success", http.StatusOK, dataTransaction)
	ctx.JSON(http.StatusOK, response)

}

// Update Transaction Handler
func (c *transactionController) UpdateTransaction(ctx *gin.Context) {
	var reqTransaction request.TransactionRequestJSON

	var transactionId request.TransactionRequestId

	ctx.ShouldBindUri(&transactionId)

	ctx.ShouldBindJSON(&reqTransaction)

	// Validate Format Request
	err := c.validate.Struct(reqTransaction)

	if err != nil {
		response := helper.APIResponse("Incomplete Data", "bad request", http.StatusBadRequest, gin.H{"errors": err.Error()})
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	dataTransaction, errResponse := c.service.UpdateTransaction(ctx, reqTransaction, transactionId.Id)

	if errResponse != nil {
		ctx.JSON(errResponse.Meta.Code, errResponse)
		return
	}

	response := helper.APIResponse("Success Update Transaction", "success", http.StatusOK, dataTransaction)
	ctx.JSON(http.StatusOK, response)

}

// Delete Transaction Handler
func (c *transactionController) DeleteTransaction(ctx *gin.Context) {

	var transactionId request.TransactionRequestId

	ctx.ShouldBindUri(&transactionId)

	err := c.service.DeleteTransaction(ctx, transactionId.Id)

	if err != nil {
		ctx.JSON(err.Meta.Code, err)
		return
	}

	response := helper.APIResponse("Success Update Transaction", "success", http.StatusOK, gin.H{"message": "Delete Success"})
	ctx.JSON(http.StatusOK, response)

}
