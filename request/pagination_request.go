package request

type PaginationRequest struct {
	Page     string
	PageSize string
}
