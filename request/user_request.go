package request

type RegisterUserInput struct {
	Username string `json:"user" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

type LoginUserInput struct {
	Username string `json:"user" validate:"required"`
	Password string `json:"password" validate:"required"`
}
