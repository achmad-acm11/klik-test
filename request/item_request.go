package request

type ItemRequestId struct {
	Id int `uri:"id" binding:"required"`
}

type ItemRequestJSON struct {
	Name  string  `json:"name" validate:"required"`
	Price float32 `json:"price" validate:"required"`
	Cost  float32 `json:"cost" validate:"required"`
}
