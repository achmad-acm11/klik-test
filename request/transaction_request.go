package request

type TransactionRequestId struct {
	Id int `uri:"id" binding:"required"`
}

type TransactionRequestJSON struct {
	ItemId       int `json:"item_id" validate:"required"`
	ItemQuantity int `json:"item_quantity" validate:"required"`
}

type TransactionFilter struct {
	Date   string
	Number string
}
