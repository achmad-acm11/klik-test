package entity

// Model User
type User struct {
	Id       int
	Token    string
	Username string
	Email    string
	Password string
}
