package entity

import "gorm.io/gorm"

type TransactionDetail struct {
	Id            int
	TransactionId int
	ItemId        int
	ItemQuantity  int
	ItemPrice     float32
	ItemCost      float32
	DeletedAt     gorm.DeletedAt
	Item          Item `gorm:"ForeignKey: ItemId"`
}
