package entity

import (
	"time"

	"gorm.io/gorm"
)

type Transaction struct {
	Id                int
	Number            string
	Date              time.Time
	PriceTotal        float32
	CostTotal         float32
	DeletedAt         gorm.DeletedAt
	TransactionDetail TransactionDetail `gorm:"ForeignKey: TransactionId"`
}
