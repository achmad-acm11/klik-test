package entity

import "gorm.io/gorm"

// Model Item
type Item struct {
	Id        int
	Name      string
	Price     float32
	Cost      float32
	DeletedAt gorm.DeletedAt
}
