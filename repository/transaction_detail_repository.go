package repository

import (
	"klika2c-test/entity"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type TransactionDetailRepository interface {
	GetAll(cx *gin.Context, tx *gorm.DB) ([]entity.TransactionDetail, error)
	GetById(cx *gin.Context, tx *gorm.DB, transactionDetailId int) (entity.TransactionDetail, error)
	GetByTransactionId(cx *gin.Context, tx *gorm.DB, transactionId int) (entity.TransactionDetail, error)
	Create(cx *gin.Context, tx *gorm.DB, transaction entity.TransactionDetail) (entity.TransactionDetail, error)
	Update(cx *gin.Context, tx *gorm.DB, item entity.TransactionDetail) (entity.TransactionDetail, error)
	Delete(cx *gin.Context, tx *gorm.DB, item entity.TransactionDetail) error
}
type transactionDetailRepository struct {
}

func NewTransactionDetailRepository() *transactionDetailRepository {
	return &transactionDetailRepository{}
}

// Query All Transactions Detail
func (r *transactionDetailRepository) GetAll(cx *gin.Context, tx *gorm.DB) ([]entity.TransactionDetail, error) {
	transactionsDetail := []entity.TransactionDetail{}

	err := tx.WithContext(cx).Find(&transactionsDetail).Error

	if err != nil {
		return transactionsDetail, err
	}

	return transactionsDetail, nil
}

// Query One Transaction Detail
func (r *transactionDetailRepository) GetById(cx *gin.Context, tx *gorm.DB, transactionDetailId int) (entity.TransactionDetail, error) {
	transactionDetail := entity.TransactionDetail{}

	err := tx.WithContext(cx).Where("id = ?", transactionDetailId).Find(&transactionDetail).Error

	if err != nil {
		return transactionDetail, err
	}

	return transactionDetail, nil
}

// Query One Transaction Detail
func (r *transactionDetailRepository) GetByTransactionId(cx *gin.Context, tx *gorm.DB, transactionId int) (entity.TransactionDetail, error) {
	transactionDetail := entity.TransactionDetail{}

	err := tx.WithContext(cx).Where("transaction_id = ?", transactionId).Find(&transactionDetail).Error

	if err != nil {
		return transactionDetail, err
	}

	return transactionDetail, nil
}

// Create Transaction Detail
func (r *transactionDetailRepository) Create(cx *gin.Context, tx *gorm.DB, transactionDetail entity.TransactionDetail) (entity.TransactionDetail, error) {
	err := tx.WithContext(cx).Create(&transactionDetail).Error

	if err != nil {
		return transactionDetail, err
	}

	return transactionDetail, nil
}

// Update Transaction Detail
func (r *transactionDetailRepository) Update(cx *gin.Context, tx *gorm.DB, transactionDetail entity.TransactionDetail) (entity.TransactionDetail, error) {
	err := tx.WithContext(cx).Save(&transactionDetail).Error

	if err != nil {
		return transactionDetail, err
	}

	return transactionDetail, nil
}

// Delete Transaction Detail
func (r *transactionDetailRepository) Delete(cx *gin.Context, tx *gorm.DB, transactionDetail entity.TransactionDetail) error {
	err := tx.WithContext(cx).Delete(&transactionDetail).Error

	if err != nil {
		return err
	}

	return nil
}
