package repository

import (
	"klika2c-test/entity"
	"klika2c-test/helper"
	"klika2c-test/request"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type TransactionRepository interface {
	GetAll(cx *gin.Context, tx *gorm.DB, pagination request.PaginationRequest, filter request.TransactionFilter) ([]entity.Transaction, error)
	GetById(cx *gin.Context, tx *gorm.DB, transactionId int) (entity.Transaction, error)
	Create(cx *gin.Context, tx *gorm.DB, transaction entity.Transaction) (entity.Transaction, error)
	Update(cx *gin.Context, tx *gorm.DB, item entity.Transaction) (entity.Transaction, error)
	Delete(cx *gin.Context, tx *gorm.DB, item entity.Transaction) error
}
type transactionRepository struct {
}

func NewTransactionRepository() *transactionRepository {
	return &transactionRepository{}
}

// Query All Transactions
func (r *transactionRepository) GetAll(cx *gin.Context, tx *gorm.DB, pagination request.PaginationRequest, filter request.TransactionFilter) ([]entity.Transaction, error) {
	transactions := []entity.Transaction{}

	query := tx.WithContext(cx).Preload("TransactionDetail").Scopes(helper.Paginate(pagination.Page, pagination.PageSize))

	if filter.Date != "" {
		date, _ := time.Parse("02-01-2006", filter.Date)
		query = query.Where("date like ?", date.Format("2006-01-02")+"%")
	}
	if filter.Number != "" {
		query = query.Where("number = ?", filter.Number)
	}

	err := query.Find(&transactions).Error

	if err != nil {
		return transactions, err
	}

	return transactions, nil
}

// Query One Transaction
func (r *transactionRepository) GetById(cx *gin.Context, tx *gorm.DB, transactionId int) (entity.Transaction, error) {
	transaction := entity.Transaction{}

	err := tx.WithContext(cx).Where("id = ?", transactionId).Preload("TransactionDetail").Find(&transaction).Error

	if err != nil {
		return transaction, err
	}

	return transaction, nil
}

// Create Transaction
func (r *transactionRepository) Create(cx *gin.Context, tx *gorm.DB, transaction entity.Transaction) (entity.Transaction, error) {
	err := tx.WithContext(cx).Create(&transaction).Error

	if err != nil {
		return transaction, err
	}

	return transaction, nil
}

// Update Transaction
func (r *transactionRepository) Update(cx *gin.Context, tx *gorm.DB, transaction entity.Transaction) (entity.Transaction, error) {
	err := tx.WithContext(cx).Save(&transaction).Error

	if err != nil {
		return transaction, err
	}

	return transaction, nil
}

// Delete Transaction
func (r *transactionRepository) Delete(cx *gin.Context, tx *gorm.DB, transaction entity.Transaction) error {
	err := tx.WithContext(cx).Delete(&transaction).Error

	if err != nil {
		return err
	}

	return nil
}
