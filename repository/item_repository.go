package repository

import (
	"klika2c-test/entity"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ItemRepository interface {
	GetAll(cx *gin.Context, tx *gorm.DB) ([]entity.Item, error)
	GetById(cx *gin.Context, tx *gorm.DB, itemId int) (entity.Item, error)
	Create(cx *gin.Context, tx *gorm.DB, item entity.Item) (entity.Item, error)
	Update(cx *gin.Context, tx *gorm.DB, item entity.Item) (entity.Item, error)
	Delete(cx *gin.Context, tx *gorm.DB, item entity.Item) error
}
type itemRepository struct {
}

func NewItemRepository() *itemRepository {
	return &itemRepository{}
}

// Query All Items
func (r *itemRepository) GetAll(cx *gin.Context, tx *gorm.DB) ([]entity.Item, error) {
	items := []entity.Item{}

	err := tx.WithContext(cx).Find(&items).Error

	if err != nil {
		return items, err
	}

	return items, nil
}

// Query One Item
func (r *itemRepository) GetById(cx *gin.Context, tx *gorm.DB, itemId int) (entity.Item, error) {
	item := entity.Item{}

	err := tx.WithContext(cx).Where("id = ?", itemId).Find(&item).Error

	if err != nil {
		return item, err
	}

	return item, nil
}

// Create Item
func (r *itemRepository) Create(cx *gin.Context, tx *gorm.DB, item entity.Item) (entity.Item, error) {
	err := tx.WithContext(cx).Create(&item).Error

	if err != nil {
		return item, err
	}

	return item, nil
}

// Update Item
func (r *itemRepository) Update(cx *gin.Context, tx *gorm.DB, item entity.Item) (entity.Item, error) {
	err := tx.WithContext(cx).Save(&item).Error

	if err != nil {
		return item, err
	}

	return item, nil
}

// Delete Item
func (r *itemRepository) Delete(cx *gin.Context, tx *gorm.DB, item entity.Item) error {
	err := tx.WithContext(cx).Delete(&item).Error

	if err != nil {
		return err
	}

	return nil
}
