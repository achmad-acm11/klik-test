package repository

import (
	"klika2c-test/entity"

	"gorm.io/gorm"
)

type UserRepository interface {
	Create(user entity.User) (entity.User, error)
	Update(user entity.User) (entity.User, error)
	GetById(id int) (entity.User, error)
	GetByUsername(user entity.User) (entity.User, error)
}

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *userRepository {
	return &userRepository{
		db: db,
	}
}

// Query Create Data
func (r *userRepository) Create(user entity.User) (entity.User, error) {
	err := r.db.Create(&user).Error

	if err != nil {
		return user, err
	}

	return user, nil
}

// Query Update User
func (r *userRepository) Update(user entity.User) (entity.User, error) {
	err := r.db.Save(&user).Error

	if err != nil {
		return user, err
	}

	return user, nil
}

// Query Get User By Email
func (r *userRepository) GetByUsername(user entity.User) (entity.User, error) {
	var userRes entity.User

	err := r.db.Where("username = ?", user.Username).Find(&userRes).Error

	if err != nil {
		return userRes, err
	}

	return userRes, nil
}

// Query Get Data By Id
func (r *userRepository) GetById(id int) (entity.User, error) {
	var userRes entity.User

	err := r.db.Where("id = ?", id).Find(&userRes).Error

	if err != nil {
		return userRes, err
	}

	return userRes, nil
}
