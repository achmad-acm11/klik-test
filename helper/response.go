package helper

type Response struct {
	Meta Meta `json:"meta"`
	Data interface{}
}
type Meta struct {
	Message string `json:"message"`
	Status  string `json:"status"`
	Code    int    `json:"code"`
}

func APIResponse(message string, status string, code int, data interface{}) Response {
	return Response{
		Meta: Meta{
			Message: message,
			Status:  status,
			Code:    code,
		},
		Data: data,
	}
}

func APIResponseError(message string, status string, code int, data interface{}) *Response {
	return &Response{
		Meta: Meta{
			Message: message,
			Status:  status,
			Code:    code,
		},
		Data: data,
	}
}
