package response

import "klika2c-test/entity"

type TransactionResponse struct {
	Id                        int                       `json:"id"`
	Number                    string                    `json:"number"`
	Date                      string                    `json:"date"`
	PriceTotal                float32                   `json:"price_total"`
	CostTotal                 float32                   `json:"cost_total"`
	TransactionDetailResponse TransactionDetailResponse `json:"detail"`
}

type TransactionDetailResponse struct {
	Id           int `json:"id"`
	ItemQuantity int `json:"item_quantity"`
	ItemPrice    int `json:"item_price"`
	ItemCost     int `json:"item_cost"`
}

func MapTransactionResponse(transaction entity.Transaction) TransactionResponse {
	return TransactionResponse{
		Id:         transaction.Id,
		Number:     transaction.Number,
		Date:       transaction.Date.Format("02-01-2006"),
		PriceTotal: transaction.PriceTotal,
		CostTotal:  transaction.CostTotal,
		TransactionDetailResponse: TransactionDetailResponse{
			Id:           transaction.TransactionDetail.Id,
			ItemQuantity: transaction.TransactionDetail.ItemQuantity,
			ItemPrice:    int(transaction.TransactionDetail.ItemPrice),
			ItemCost:     int(transaction.TransactionDetail.ItemCost),
		},
	}
}

func MapTransactionsResponse(transactions []entity.Transaction) []TransactionResponse {
	var transactionsResponse []TransactionResponse

	for _, transaction := range transactions {
		transactionsResponse = append(transactionsResponse, MapTransactionResponse(transaction))
	}

	return transactionsResponse

}
