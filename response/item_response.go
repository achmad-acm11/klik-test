package response

import "klika2c-test/entity"

type ItemResponse struct {
	Id    int     `json:"id"`
	Name  string  `json:"name"`
	Price float32 `json:"price"`
	Cost  float32 `json:"cost"`
}

func MapItemResponse(item entity.Item) ItemResponse {
	return ItemResponse{
		Id:    item.Id,
		Name:  item.Name,
		Price: item.Price,
		Cost:  item.Cost,
	}
}

func MapItemsResponse(items []entity.Item) []ItemResponse {
	var itemsResponse []ItemResponse

	for _, item := range items {
		itemsResponse = append(itemsResponse, MapItemResponse(item))
	}

	return itemsResponse

}
