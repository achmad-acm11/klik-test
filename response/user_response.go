package response

import "klika2c-test/entity"

type UserResponse struct {
	Id       int    `json:"id"`
	Token    string `json:"token"`
	Username string `json:"user"`
	Email    string `json:"email"`
}

func MapUserResponse(user entity.User) UserResponse {
	return UserResponse{
		Id:       user.Id,
		Token:    user.Token,
		Username: user.Username,
		Email:    user.Email,
	}
}

func MapUsersResponse(users []entity.User) []UserResponse {
	var usersResponse []UserResponse

	for _, user := range users {
		usersResponse = append(usersResponse, MapUserResponse(user))
	}

	return usersResponse

}
