FROM golang:1.17-stretch AS builder
WORKDIR /klika2c-test
COPY ./.env ./
COPY ./    ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM alpine:latest as golang-app
RUN apk --no-cache add ca-certificates
RUN apk add --no-cache bash
WORKDIR /root/
COPY --from=builder /klika2c-test/main ./
COPY --from=builder /klika2c-test/.env ./
CMD ["./main"]
