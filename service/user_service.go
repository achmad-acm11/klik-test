package service

import (
	"errors"
	"klika2c-test/entity"
	"klika2c-test/repository"
	"klika2c-test/request"

	"golang.org/x/crypto/bcrypt"
)

type UserService interface {
	GetUserById(id int) (entity.User, error)
	RegisterUser(reqUserRegister request.RegisterUserInput) (entity.User, error)
	UpdateUser(user entity.User) (entity.User, error)
	LoginUser(reqUserLogin request.LoginUserInput) (entity.User, error)
}

type userService struct {
	repo repository.UserRepository
}

func NewUserService(repo repository.UserRepository) *userService {
	return &userService{
		repo: repo,
	}
}

// Get Data User By Id
func (s *userService) GetUserById(id int) (entity.User, error) {
	dataUser, err := s.repo.GetById(id)

	if err != nil {
		return dataUser, err
	}
	if dataUser.Id == 0 {
		return dataUser, errors.New("no user found on with that id")
	}
	return dataUser, nil
}

// Create User Data Service
func (s *userService) RegisterUser(reqUserRegister request.RegisterUserInput) (entity.User, error) {
	// Generate Password
	passHash, err := bcrypt.GenerateFromPassword([]byte(reqUserRegister.Password), bcrypt.MinCost)

	if err != nil {
		return entity.User{}, err
	}

	user := entity.User{
		Username: reqUserRegister.Username,
		Email:    reqUserRegister.Email,
		Password: string(passHash),
	}

	// Create Data User Repo
	newUser, err := s.repo.Create(user)

	if err != nil {
		return newUser, err
	}

	return newUser, nil
}

// Login User Data Service
func (s *userService) LoginUser(reqUserLogin request.LoginUserInput) (entity.User, error) {
	user := entity.User{
		Username: reqUserLogin.Username,
	}
	// Check Email User
	dataUser, err := s.repo.GetByUsername(user)

	if err != nil {
		return dataUser, err
	}
	// Data User is not exist
	if dataUser.Id == 0 {
		return dataUser, errors.New("login failed")
	}
	// Check Password Hash
	err = bcrypt.CompareHashAndPassword([]byte(dataUser.Password), []byte(reqUserLogin.Password))
	if err != nil {
		return dataUser, err
	}

	return dataUser, nil
}

// Update User Data Service
func (s *userService) UpdateUser(user entity.User) (entity.User, error) {
	dataUser, err := s.repo.Update(user)
	if err != nil {
		return dataUser, err
	}
	return dataUser, nil
}
