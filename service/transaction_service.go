package service

import (
	"errors"
	"fmt"
	"klika2c-test/entity"
	"klika2c-test/helper"
	"klika2c-test/repository"
	"klika2c-test/request"
	"klika2c-test/response"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type TransactionService interface {
	GetAllTransaction(ctx *gin.Context, pagination request.PaginationRequest, filter request.TransactionFilter) ([]response.TransactionResponse, *helper.Response)
	GetOneTransaction(ctx *gin.Context, transactionId int) (response.TransactionResponse, *helper.Response)
	CreateTransaction(ctx *gin.Context, reqTransaction request.TransactionRequestJSON) (response.TransactionResponse, *helper.Response)
	UpdateTransaction(ctx *gin.Context, reqTransaction request.TransactionRequestJSON, transactionId int) (response.TransactionResponse, *helper.Response)
	DeleteTransaction(ctx *gin.Context, transactionId int) *helper.Response
}

type transactionService struct {
	repo       repository.TransactionRepository
	repoItem   repository.ItemRepository
	repoDetail repository.TransactionDetailRepository
	db         *gorm.DB
}

func NewTransactionService(repo repository.TransactionRepository, repoItem repository.ItemRepository, repoDetail repository.TransactionDetailRepository, db *gorm.DB) *transactionService {
	return &transactionService{
		repo:       repo,
		repoItem:   repoItem,
		repoDetail: repoDetail,
		db:         db,
	}
}

// Service Get All Transaction
func (s *transactionService) GetAllTransaction(ctx *gin.Context, pagination request.PaginationRequest, filter request.TransactionFilter) ([]response.TransactionResponse, *helper.Response) {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	transactions, err := s.repo.GetAll(ctx, tx, pagination, filter)

	if err != nil {
		return nil, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	return response.MapTransactionsResponse(transactions), nil
}

// Service Get One Transaction By Id
func (s *transactionService) GetOneTransaction(ctx *gin.Context, transactionId int) (response.TransactionResponse, *helper.Response) {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	transaction, err := s.repo.GetById(ctx, tx, transactionId)

	if err != nil {
		return response.TransactionResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	if transaction.Id == 0 {
		return response.TransactionResponse{}, helper.APIResponseError("Not Found", "error", http.StatusNotFound, gin.H{"errors": errors.New("transaction not found").Error()})
	}

	return response.MapTransactionResponse(transaction), nil
}

// Service Create Transaction
func (s *transactionService) CreateTransaction(ctx *gin.Context, reqTransaction request.TransactionRequestJSON) (response.TransactionResponse, *helper.Response) {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	// Get Item Data
	dataItem, err := s.repoItem.GetById(ctx, tx, reqTransaction.ItemId)

	if err != nil {
		return response.TransactionResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	if dataItem.Id == 0 {
		return response.TransactionResponse{}, helper.APIResponseError("Not Found", "error", http.StatusNotFound, gin.H{"errors": errors.New("item not found").Error()})
	}

	// Create Transaction Header
	rand.Seed(time.Now().UTC().UnixNano())
	min := 1
	max := 99999999
	number := fmt.Sprintf("%s%s-%s", strconv.Itoa(time.Now().Year()), strconv.Itoa(int(time.Now().Month())), fmt.Sprintf("%08d", (rand.Intn(max-min)+min)))
	priceTotal := float32(reqTransaction.ItemQuantity) * dataItem.Price
	costTotal := float32(reqTransaction.ItemQuantity) * dataItem.Cost

	transaction, err := s.repo.Create(ctx, tx, entity.Transaction{
		Number:     number,
		Date:       time.Now(),
		PriceTotal: priceTotal,
		CostTotal:  costTotal,
	})
	if err != nil {
		return response.TransactionResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	// Create Transaction Detail
	transactionDetail, err := s.repoDetail.Create(ctx, tx, entity.TransactionDetail{
		TransactionId: transaction.Id,
		ItemId:        dataItem.Id,
		ItemQuantity:  reqTransaction.ItemQuantity,
		ItemPrice:     dataItem.Price,
		ItemCost:      dataItem.Cost,
	})

	if err != nil {
		return response.TransactionResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	transaction.TransactionDetail.Id = transactionDetail.Id
	transaction.TransactionDetail.TransactionId = transactionDetail.TransactionId
	transaction.TransactionDetail.ItemId = transactionDetail.ItemId
	transaction.TransactionDetail.ItemQuantity = transactionDetail.ItemQuantity
	transaction.TransactionDetail.ItemPrice = transactionDetail.ItemPrice
	transaction.TransactionDetail.ItemCost = transactionDetail.ItemCost

	return response.MapTransactionResponse(transaction), nil
}

// Service Update Transaction
func (s *transactionService) UpdateTransaction(ctx *gin.Context, reqTransaction request.TransactionRequestJSON, transactionId int) (response.TransactionResponse, *helper.Response) {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	// Get Item Data
	dataItem, err := s.repoItem.GetById(ctx, tx, reqTransaction.ItemId)

	if err != nil {
		return response.TransactionResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	if dataItem.Id == 0 {
		return response.TransactionResponse{}, helper.APIResponseError("Not Found", "error", http.StatusNotFound, gin.H{"errors": errors.New("item not found").Error()})
	}

	transactionData, err := s.repo.GetById(ctx, tx, transactionId)

	if err != nil {
		return response.TransactionResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	if transactionData.Id == 0 {
		return response.TransactionResponse{}, helper.APIResponseError("Not Found", "error", http.StatusNotFound, gin.H{"errors": errors.New("transaction not found").Error()})
	}

	transactionData.TransactionDetail.ItemId = dataItem.Id
	transactionData.TransactionDetail.ItemQuantity = reqTransaction.ItemQuantity
	transactionData.TransactionDetail.ItemPrice = dataItem.Price
	transactionData.TransactionDetail.ItemCost = dataItem.Cost
	transactionData.PriceTotal = float32(reqTransaction.ItemQuantity) * dataItem.Price
	transactionData.CostTotal = float32(reqTransaction.ItemQuantity) * dataItem.Cost

	transaction, err := s.repo.Update(ctx, tx, transactionData)

	if err != nil {
		return response.TransactionResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	return response.MapTransactionResponse(transaction), nil
}

// Service Delete Transaction
func (s *transactionService) DeleteTransaction(ctx *gin.Context, transactionId int) *helper.Response {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	transactionData, err := s.repo.GetById(ctx, tx, transactionId)

	if err != nil {
		return helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	if transactionData.Id == 0 {
		return helper.APIResponseError("Not Found", "error", http.StatusNotFound, gin.H{"errors": errors.New("transaction not found").Error()})
	}

	s.repo.Delete(ctx, tx, transactionData)

	detailData, err := s.repoDetail.GetByTransactionId(ctx, tx, transactionId)

	if err != nil {
		return helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	if detailData.Id == 0 {
		return helper.APIResponseError("Not Found", "error", http.StatusNotFound, gin.H{"errors": errors.New("detail not found").Error()})
	}

	s.repoDetail.Delete(ctx, tx, detailData)

	return nil
}
