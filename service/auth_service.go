package service

import (
	"errors"

	"github.com/dgrijalva/jwt-go"
)

type AuthService interface {
	GenerateToken(user_id int) (string, error)
	ValidateToken(token string) (*jwt.Token, error)
}
type authJwt struct {
}

func NewAuthService() *authJwt {
	return &authJwt{}
}
func (s *authJwt) GenerateToken(id int) (string, error) {
	claim := jwt.MapClaims{
		"id": id,
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claim).SignedString([]byte("secret_test"))

	if err != nil {
		return token, err
	}

	return token, nil
}

func (s *authJwt) ValidateToken(t string) (*jwt.Token, error) {
	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, errors.New("invalid token")
		}
		return []byte("secret_test"), nil
	})

	if err != nil {
		return token, err
	}

	return token, nil
}
