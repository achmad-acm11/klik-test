package service

import (
	"errors"
	"klika2c-test/entity"
	"klika2c-test/helper"
	"klika2c-test/repository"
	"klika2c-test/request"
	"klika2c-test/response"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ItemService interface {
	GetAllItem(ctx *gin.Context) ([]response.ItemResponse, *helper.Response)
	GetOneItem(ctx *gin.Context, itemId int) (response.ItemResponse, *helper.Response)
	CreateItem(ctx *gin.Context, reqItem request.ItemRequestJSON) (response.ItemResponse, *helper.Response)
	UpdateItem(ctx *gin.Context, reqItem request.ItemRequestJSON, itemId int) (response.ItemResponse, *helper.Response)
	DeleteItem(ctx *gin.Context, itemId int) *helper.Response
}

type itemService struct {
	repo repository.ItemRepository
	db   *gorm.DB
}

func NewItemService(repo repository.ItemRepository, db *gorm.DB) *itemService {
	return &itemService{repo: repo, db: db}
}

// Service Get All Item
func (s *itemService) GetAllItem(ctx *gin.Context) ([]response.ItemResponse, *helper.Response) {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	items, err := s.repo.GetAll(ctx, tx)

	if err != nil {
		return nil, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	return response.MapItemsResponse(items), nil
}

// Service Get One Item By Id
func (s *itemService) GetOneItem(ctx *gin.Context, itemId int) (response.ItemResponse, *helper.Response) {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	item, err := s.repo.GetById(ctx, tx, itemId)

	if err != nil {
		return response.ItemResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	if item.Id == 0 {
		return response.ItemResponse{}, helper.APIResponseError("Not Found", "error", http.StatusNotFound, gin.H{"errors": errors.New("item not found").Error()})
	}

	return response.MapItemResponse(item), nil
}

// Service Create Item
func (s *itemService) CreateItem(ctx *gin.Context, reqItem request.ItemRequestJSON) (response.ItemResponse, *helper.Response) {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	item, err := s.repo.Create(ctx, tx, entity.Item{
		Name:  reqItem.Name,
		Price: reqItem.Price,
		Cost:  reqItem.Cost,
	})

	if err != nil {
		return response.ItemResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	return response.MapItemResponse(item), nil
}

// Service Update Item
func (s *itemService) UpdateItem(ctx *gin.Context, reqItem request.ItemRequestJSON, itemId int) (response.ItemResponse, *helper.Response) {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	itemData, err := s.repo.GetById(ctx, tx, itemId)

	if err != nil {
		return response.ItemResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	if itemData.Id == 0 {
		return response.ItemResponse{}, helper.APIResponseError("Not Found", "error", http.StatusNotFound, gin.H{"errors": errors.New("item not found").Error()})
	}

	itemData.Name = reqItem.Name
	itemData.Price = reqItem.Price
	itemData.Cost = reqItem.Cost

	item, err := s.repo.Update(ctx, tx, itemData)

	if err != nil {
		return response.ItemResponse{}, helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	return response.MapItemResponse(item), nil
}

// Service Delete Item
func (s *itemService) DeleteItem(ctx *gin.Context, itemId int) *helper.Response {
	// Start Transaction
	tx := s.db.Begin()

	// Handle Transaction
	defer helper.CommitOrRollback(tx)

	itemData, err := s.repo.GetById(ctx, tx, itemId)

	if err != nil {
		return helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	if itemData.Id == 0 {
		return helper.APIResponseError("Not Found", "error", http.StatusNotFound, gin.H{"errors": errors.New("item not found").Error()})
	}

	err = s.repo.Delete(ctx, tx, itemData)

	if err != nil {
		return helper.APIResponseError("Internal Server Error", "error", http.StatusInternalServerError, gin.H{"errors": err.Error()})
	}

	return nil
}
